//draw score
if(!playerFlying) {
    draw_text_ext(5, 5, "SCORE: " + string(score), 3, 500);
    draw_text_ext(5, 23, "LIVES: " + string(lives), 3, 500);
    draw_text_ext(5, 41, "DO-OVERS: " + string(Player.mulligansTaken), 3, 300);
}

randomize();

REFEREE_ALARM = 150;
gameOver = false;

lives = 15;

playerFlying = false;
currLineIndex = 0;

textLines[0] = "Congratulations.";
textLines[1] = "I didn't think I'd make it here";
textLines[2] = "but here we are.";
textLines[3] = "Reaching, stretching...";
textLines[4] = "floating, really.";
textLines[5] = "To what? I can't say.";
textLines[6] = "Anything's possible,";
textLines[7] = "And it's early yet.";

draw_set_color(c_white);

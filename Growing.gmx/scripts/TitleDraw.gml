draw_self();

draw_text_ext(20, 450, 'Music:#"Bit Quest" and "Deliberate Thought"#by Kevin MacLeod (incompetech.com)#Licensed under Creative Commons#By Attribution 3.0 License#http://creativecommons.org/licenses/by/3.0/', 18, 600);

draw_text_ext(400, 450,
    'Any key, click or tap to launch.#Press again while in the air to take a mulligan.',
    18, 400);

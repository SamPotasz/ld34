if(!playerFlying && Player.y < 2000) {
    playerFlying = true;
    Player.vspeed = -1.5;
    
    alarm[0]=REFEREE_ALARM;
    
    if(audio_is_playing(BitQuest)){
        audio_stop_sound(BitQuest);
    }
    audio_play_sound(DeliberateThought, 1, true);
}

if(currLineIndex <= 7) {
    var currLine = textLines[currLineIndex];
    var textX = random(525) + 30;
    var newText = instance_create(textX, Player.y, TextDisplayer);
    with(newText) {
        toDisplay = currLine;
    }  

    currLineIndex++;
    alarm[0] = REFEREE_ALARM;
}
else {
    gameOver = true;
    with(TextDisplayer) {
        instance_destroy();
    }
    var newText = instance_create(300, 200, TextDisplayer);
    with(newText) {
        toDisplay = "Play Again?";
    }  
}
